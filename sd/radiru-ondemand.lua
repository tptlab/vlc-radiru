--[[
 Add NHK radio ondemand (Radiru * Radiru) as a service.
 The structure is borrowed from README in VLC
 https://www.videolan.org/developers/vlc/share/lua/sd/README.txt

 Copyright © 2019 caplas

 Authors: caplas <rad164@teapot-lab.space>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
--]]

lazily_loaded = false
dkjson        = nil

json_url = "https://www.nhk.or.jp/radioondemand/json/index_v3/index.json"

function lazy_load()
  if lazily_loaded then return nil end
  dkjson = require("dkjson")
  lazily_loaded = true
end

function descriptor()
  return { title = "らじる★らじる 聴き逃し" }
end

function main()
  lazy_load()
  local fp, err = vlc.stream(json_url)
  if not fp then
    if not err then err = "Unknown error when opening stream" end
    vlc.msg.err(string.format("NHK: %s, %s", err, json_url))
    return
  end
  local jsonstr = ""
  local line   = ""
  while true do
    line = fp:readline()
    if not line then
      break
    end
    jsonstr = jsonstr .. line
  end

  local index, _, err = dkjson.decode(jsonstr)
  if not index then
    if not err then err = "Unknown error when parsing json" end
    vlc.msg.err(string.format("NHK: %s, %s", err, json_url))
    return
  end

  for _, program in ipairs(index.data_list) do
    local name = program.program_name
    if program.corner_name then
      name = name .. " " .. program.corner_name
    end

    vlc.sd.add_item(
      { path = program.detail_json, title = name }
    )
  end
end
