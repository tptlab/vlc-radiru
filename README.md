Add NHK radio on-demand "Radiru-Radiru Kikinogashi" to VLC sidebar.

# らじる聴き逃し for VLC

NHK ラジオの「らじる★らじる 聴き逃し」を VLC のサイドバーに追加します。

注意: 当プラグインは非公式です。先方の仕様変更等により使えなくなる可能性があります。

## インストール

下記のディレクトリに `sd` と `playlist` という名前のディレクトリを作り、それぞれにスクリプトを配置してください。

- Windows: `%APPDATA%VLC\lua\`
- mac: `~/Library/Application Support/org.videolan.vlc/lua/`
- Linux: `~/.local/share/vlc/lua/`

mac と Linux では同梱の install.sh が使えます。

## 既知の不具合

### 番組名をダブルクリックするとエラーダイアログが出る

(英語)

> Your input can't be opened:
> VLC is unable to open the MRL 'https://(..URL..).json'. Check the log for details.

(日本語)

> 入力を開くことができません:
> VLCはMRL 'https://(..URL..).json'を開けません。詳細はログを確認してください。

サーバー側の不具合やタイミングの問題により、読み込みが失敗することがあります。
ログに下記の行があればこれに該当します。

> http error: peer stream 1 error: Cancellation (0x8)

サーバー側の問題なので対処法はなく、他のネットラジオやリモートの動画を開く場合でも起きます。VLC を再起動したり時間を置いてから再度試してください。
